#!/bin/bash

sshd-keygen || true
/usr/sbin/sshd -D &

mkdir /root/.ssh
chown root: /root/.ssh
echo $SSH_PUBLIC_KEY > /root/.ssh/authorized_keys
unset SSH_PUBLIC_KEY
chown root: /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

/usr/sbin/httpd -D FOREGROUND
